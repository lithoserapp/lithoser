const React = require("react-native");

const { StyleSheet, Dimensions, Platform } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#FBFAFA',
  },
  imageContainer: {
    flex: 1,
    width: deviceWidth,
    height: deviceHeight
  },
  logoContainer: {
    flex: 1,
    marginTop: deviceHeight / 10,
    marginBottom: 30
  },
  logo: {
    position: "absolute",
    left: Platform.OS === "android" ? 50 : 50,
    top: Platform.OS === "android" ? 35 : 60,
    width: 310,
    height: 160
  },
  text: {
    color: "#D8D8D8",
    bottom: 6,
    marginTop: 5
  },
  contentContainer: {
    flex: 1,
    marginTop: deviceHeight / 4,
    paddingLeft: 10,
    paddingRight: 10,
  },
  input: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
  },
  btn: {
    marginTop: 20,
    alignSelf: 'center',
  },
};
