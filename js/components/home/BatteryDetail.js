import React from 'react';
import { Alert } from 'react-native';
import { Text, Left, Body, Button, List, ListItem,
   Right, Thumbnail, Icon } from 'native-base';
import * as Progress from 'react-native-progress';
import styles from './styles';

const defaultBatteryImage = require('../../../img/battery.png');

const ReactNative = require("react-native");
const { Dimensions, Platform } = ReactNative;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const BatteryDetail = ({ batteryProp, onBatteryItemDelete, onBatteryItemDetail }) => {
  let progress = 0.3;
  const timeDiff = Math.abs(batteryProp.installedDate - new Date());
  const secDiff = Math.ceil(timeDiff / 1000);
  progress = secDiff / (batteryProp.lifeExpectancyInDays * 24 * 3600) > 1
             ? 0
             : (1 - (secDiff / (batteryProp.lifeExpectancyInDays * 24 * 3600))) ;
  let color = 'red';
  if (progress > 0.6) {
    color = 'green';
  } else if (progress < 0.6 && progress > 0.3) {
    color = 'orange';
  } else if (progress < 0.3) {
    color = 'red';
  }
  const progressInPercentage = (progress * 100).toFixed(4);
  const currentBatteryImage = batteryProp.batteryImage || defaultBatteryImage;
  const onItemDetailPress = () => {
    onBatteryItemDetail(batteryProp.saleDetailsId);
  };
  const onItemDeletePress = () => {
    onBatteryItemDelete(batteryProp.saleDetailsId);
  };
  return (
    <List>
      <ListItem avatar itemDivide button onPress={onItemDetailPress}>
        <Left>
          <Thumbnail square source={currentBatteryImage} />
        </Left>
        <Body>
          <Text>Machine: {batteryProp.batteryName}</Text>
          <Text note>Battery life: {progressInPercentage}%</Text>
          <Progress.Bar
            progress={progress}
            width={deviceWidth / 2}
            height={10}
            color={color}
          />
        </Body>
        <Right>
          <Text note> Qty: {batteryProp.quantity} </Text>
          <Button
            iconRight small transparent warning onPress={onItemDeletePress}
            style={styles.trashBtn}
          >
            <Icon active name={'trash'} />
          </Button>
        </Right>
      </ListItem>
    </List>
  );
};
BatteryDetail.propTypes = {
  onBatteryItemDelete: React.PropTypes.func.isRequired,
  batteryProp: React.PropTypes.object.isRequired,
};
export default BatteryDetail;
