/* @flow */

import React from "react";
import { DrawerNavigator } from "react-navigation";

import Home from "./components/home/";
import Login from "./components/login/";

const Drawer = DrawerNavigator(
  {
    Login: { screen: Login },
    Home: { screen: Home },

  },
  {
    initialRouteName: "Login",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
  }
);

export default Drawer;
