const React = require("react-native");

const { StyleSheet, Dimensions, Platform } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  logoContainer: {
    flex: 1,
    marginTop: deviceHeight / 8,
    marginBottom: 30
  },
  logo: {
    position: "absolute",
    left: Platform.OS === "android" ? 40 : 50,
    top: Platform.OS === "android" ? 35 : 60,
    width: 280,
    height: 100
  },
  container: {
    backgroundColor: '#FBFAFA',
  },
  row: {
    flex: 1,
    alignItems: 'center',
  },
  text: {
    fontSize: 20,
    marginBottom: 15,
    alignSelf: 'center',
  },
  mt: {
    marginTop: 18,
  },
  iconStyle:{
    color: '#0A69FE',
  },
  dateIconStyle: {
    position: 'absolute',
    left: 0,
    top: 4,
    marginLeft: 0,
  },
  btn: {
    alignSelf: 'flex-start',
  },
  trashBtn: {
    left: 5,
    marginTop: 5,
    alignSelf: 'flex-start',
  },
  thumbnailStyle: {
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 10,
  },
  refreshViewStyle: {
    alignSelf: 'center',
  },
  pickerStyle:{
    width: Platform.OS === "ios" ? undefined : 160
  }
};
