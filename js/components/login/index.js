import React, { Component } from "react";
import { Image, View, StatusBar, Keyboard } from "react-native";

import { Container, Content, Item, Input, Button, Icon,
	 Text, Picker, Spinner } from 'native-base';
import axios from 'axios';
import styles from "./styles";

const loginScreenBackground = require('../../../img/Status-battery-charging-caution-icon_large.png');
const appConfig = require('../../app-config.json');
const phoneNumberLength = 10;

const phoneNumberPattern = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			phoneNumber: '',
			firstName: '',
			profileId: undefined,
			loading: false,
			error: '',
		};
	}


	onLoginFail() {
		this.setState({
			phoneNumber: '',
			firstName: '',
			profileId: '',
			loading: false,
			error: 'Authentication failed',
		});
		Alert.alert('Warning', 'Authentication failed or network is unavailable.');
	}

	onLoginSuccess() {
		this.setState({
			phoneNumber: '',
			firstName: '',
			profileId: '',
			loading: false,
			error: '',
		});
	}


  onLoginPress() {
    const { phoneNumber } = this.state;
    if (phoneNumber.length !== phoneNumberLength
      && !phoneNumberPattern.test(phoneNumber)) {
      Alert.alert('Warning', 'Invalid phone number.');
      return;
    }
    this.setState({
      loading: true,
      error: '',
    });

    this.fetchProfile();
  }



  fetchProfile() {
    axios.get(`${appConfig.lithoserBaseURL}/lithoser/profile/detailsByPhoneNumber/${
      this.state.phoneNumber}/T`)
        .then((response) => {
          const profileDetails = response.data.profileDetails;
          this.onLoginSuccess();
					this.props.navigation.navigate('Home', { profileDetails });
        })
        .catch(() => {
          this.onLoginFail();
        }
      );
  }

  renderLoginButton() {
    if (this.state.loading) {
      return <Spinner size="large" color="blue" />;
    }

    return (
      <Button iconLeft rounded style={styles.btn} onPress={() => { this.onLoginPress(); }} >
        <Icon name="log-in" />
        <Text>Login</Text>
      </Button>
    );
  }
	render() {
		return (
				<Container>
					<Image source={loginScreenBackground} style={styles.imageContainer}>
						<Content style={styles.contentContainer}>
							<Item style={styles.input}>
								<Icon active name="person" />
								<Input
									placeholder="Phone#"
									value={this.state.phoneNumber}
									onChangeText={phoneNumber =>
										{
											this.setState({ phoneNumber });
											if (phoneNumber.length == phoneNumberLength) {
												Keyboard.dismiss();
											}
										}
									}
									keyboardType="numeric"
                  maxLength={10}
									onSubmitEditing={Keyboard.dismiss}
								/>
							</Item>
							{ this.renderLoginButton() }
						</Content>
					</Image>
				</Container>
		);
	}
}

export default Login;
