import React, { Component } from "react";
import { TouchableOpacity, Alert, ScrollView, Platform, Dimensions } from 'react-native';

import { Container, Header, Title, Content, View, Text, Button, Icon, Left, Body, Picker,
  Item, List, ListItem, InputGroup, Input, Right, Footer, FooterTab, Thumbnail, Spinner,
  Label, Form, Toast } from 'native-base';
import axios from 'axios';
import DatePicker from 'react-native-datepicker';

import styles from "./styles";
import BatteryDetail from './BatteryDetail';

const appConfig = require('../../app-config.json');
const smallBatteryIcon = require('../../../img/battery.png');

class Home extends Component {
	constructor(props) {
		super(props);
		const profileDetails = props.navigation.state.params.profileDetails;
		this.state = {
      batteryListPage: true,
      newCustomer: false,
      profilePage: false,
      settingPage: false,
			profileId: profileDetails.profileId,
			phoneNumber: profileDetails.phoneNumber,
      firstName: profileDetails.firstName,
      middleName: profileDetails.middleName,
      lastName: profileDetails.lastName,
      userName: profileDetails.firstName +' '+ profileDetails.middleName + ' ' + profileDetails.lastName,
      emailId: profileDetails.emailId,
      addressStreetName: profileDetails.addressStreetName,
      cityZipcode: profileDetails.cityZipcode,
      sex: profileDetails.sex,
      batteryList: [],
      loading: false,
      error: '',
      machineBatteryName: '',
    };
  }
  componentWillMount() {
    this.onBatteryListLoad();
  }

  onLoadFail() {
    this.setState({
      loading: false,
      error: 'Service call failed',
    });
  }

  onLoadSuccess(response) {
    this.setState({
      batteryList: response.data.batterySaleDetailsList,
      loading: false,
      error: '',
    });
  }

  onBatteryItemLoadSuccess(response) {
    let installedDate = new Date(response.installedDate);
    this.setState({
      loading: false,
      error: '',
      batteryName: response.batteryName,
      installedDate,
      batteryLife: response.lifeExpectancyInDays.toString(),
      batteryModel: response.batteryModel,
      batteryQty: response.quantity.toString(),
      saleDetailsIdForUpdate: response.saleDetailsId,
    },()=>{
      this.setNewCustomerPage(false);
    });
  }

  onBatteryItemDetail(saleDetailsId) {
    axios.get(`${appConfig.lithoserBaseURL}/lithoser/battery/saleDetails?saleDetailsId=${saleDetailsId}`)
    .then((response) => {
      console.log(response);
      this.onBatteryItemLoadSuccess(response.data.batterySaleDetailsDto);
    }
    )
    .catch((exception) => {
      console.log('Exception :');
      console.log(exception);
      this.onLoadFail();
    }
    );
  }
  onBatteryItemDelete(saleDetailsId) {
    axios.post(`${appConfig.lithoserBaseURL}/lithoser/battery/updateActiveStatusOfSale?activeStatus=N&saleDetailsId=${saleDetailsId}`)
      .then(() => {
        Toast.show({
              text: 'Deleted successfully',
              position: 'bottom',
              buttonText: 'Okay',
              type:'success',
              duration:1200,
            });
        this.onBatteryListLoad();
      }
      )
      .catch((exception) => {
        console.log('Exception :');
        console.log(exception);
      }
      );
  }
  onBatteryListLoad(sellerProfileIdParam) {
		//FIXME machineBatteryName doesn't have realtime change . Give prior change to service
    const sellerProfileId = sellerProfileIdParam || this.state.profileId;
		const machineBatteryName = this.state.machineBatteryName || '';
    axios.get(`${appConfig.lithoserBaseURL}/lithoser/battery/saleDetailsListBySeller?sellerProfileId=${sellerProfileId}&machineBatteryName=${machineBatteryName}`)
    .then((response) => {
      console.log(response);
      this.onLoadSuccess(response);
    }
    )
    .catch((exception) => {
      console.log('Exception :');
      console.log(exception);
      this.onLoadFail();
    }
    );
  }
  onBatteryInfoFormReset() {
    this.setState(
      {
        batteryName: '',
        installedDate: '',
        batteryLife: '',
        batteryModel: '',
        batteryQty: '',
      });
  }

  onSaveBatteryInfo() {
    let saleDetailsId = this.state.saleDetailsIdForUpdate || '';
    const data = {
      batteryName: this.state.batteryName || '',
      installedDate: this.state.installedDate || '',
      lifeExpectancyInDays: this.state.batteryLife || 0,
      batteryModel: this.state.batteryModel || '',
      quantity: this.state.batteryQty || 0,
      customerPhoneNumber: this.state.phoneNumber || '',
      soldByProfileId: this.state.profileId,
      saleDetailsId,
    };
    this.setState({saleDetailsIdForUpdate:''});
    if(saleDetailsId) {
      axios.post(`${appConfig.lithoserBaseURL}/lithoser/battery/updateSale/`, data)
        .then(()=>{
          Toast.show({
                text: 'Sale saved successfully',
                position: 'bottom',
                buttonText: 'Okay',
                type:'success',
                duration:1200,
              });
          this.setBatteryListPage();
        }
        )
        .catch((exception) => {
          console.log('Exception on edit BatteryInfo :');
          console.log(exception);
        }
        );
    } else {
      axios.post(`${appConfig.lithoserBaseURL}/lithoser/battery/createSale`, data)
        .then(()=>{
          Toast.show({
                text: 'Sale registered successfully',
                position: 'bottom',
                buttonText: 'Okay',
                type:'success',
                duration:1200,
              });
          this.setBatteryListPage();
        }
        )
        .catch((exception) => {
          console.log('Exception onSaveBatteryInfo :');
          console.log(exception);
        }
        );
    }
  }
  onSaveProfileInfo() {
    const userName = this.state.userName.trim();
    const nameWordCount = userName.split(' ').length;
    const userFirstName = userName.split(' ')[0]
                      ? userName.split(' ')[0].trim()
                      : '';
    const userMiddleName = nameWordCount > 2 && userName.split(' ')[1]
                      ? userName.split(' ')[1].trim()
                      : '';
    let userLastName = '';
    if (userMiddleName) {
      userLastName = userName.split(' ').slice(2, nameWordCount).join(' ');
    } else {
      userLastName = userName.split(' ').slice(1, nameWordCount).join(' ');
    }

    const data = {
      firstName: userFirstName || '',
      middleName: userMiddleName || '',
      lastName: userLastName || '',
      emailId: this.state.emailId || '',
      phoneNumber: this.state.phoneNumber,
      dateOfBirth: null,
      sex: this.state.sex || 'Other',
      addressStreetName: this.state.addressStreetName || '',
      cityZipcode: this.state.cityZipcode || '',
      profileType: 'Technician',
      isPushNotificationEnabled: 'Yes',
      isEmailNotificationEnabled: 'Yes',
      activeStatus: 'Yes',
    };

    axios.post(`${appConfig.lithoserBaseURL}/lithoser/profile/update`, data)
      .then(Toast.show({
            text: 'Profiled updated successfully',
            position: 'bottom',
            buttonText: 'Okay',
            type:'success',
            duration:1200,
          }))
      .catch((exception) => {
        console.log('Exception :');
        console.log(exception);
      }
      );
  }

  setBatteryListPage() {
    this.setState({
      batteryListPage: true,
      newCustomer: false,
      profilePage: false,
      settingPage: false,
      batteryList: [],
      loading: true,
      error: '',
    },()=>{
      this.onBatteryListLoad();
    });

  }

  setNewCustomerPage(isNewBatteryScreen) {
    if (isNewBatteryScreen) {
      this.onBatteryInfoFormReset();
    }
    this.setState({
      batteryListPage: false,
      newCustomer: true,
      profilePage: false,
      settingPage: false,
			machineBatteryName:'',
    });
  }
  setProfilePage() {
    this.setState({
      batteryListPage: false,
      newCustomer: false,
      profilePage: true,
      settingPage: false,
			machineBatteryName:'',
    });
  }
  setSettingPage() {
    this.setState({
      batteryListPage: false,
      newCustomer: false,
      profilePage: false,
      settingPage: true,
			machineBatteryName:'',
    });
  }

	renderHelper() {
    if (this.state.loading) {
      return <Spinner size="large" color="blue" />;
    }
    return this.state.batteryList.map((batteryItem, index) =>
      <BatteryDetail
        key={index}
        batteryProp={batteryItem}
        onBatteryItemDelete={this.onBatteryItemDelete.bind(this)}
        onBatteryItemDetail={this.onBatteryItemDetail.bind(this)}
      />
    );
  }
  renderContent() {
    if (this.state.batteryListPage) {
      return (
        <Container style={{height:((this.state.batteryList.length > 5 ? this.state.batteryList.length/5 : 1) * Dimensions.get("window").height ) }}>
          <Header searchBar rounded >
							<Item>
							 <Icon name="search" />
					     <Input placeholder="Search by Machine name"
			 					maxLength={10}
								value={this.state.machineBatteryName}
								onChangeText={
															(machineBatteryName) =>
															{
																this.setState({ machineBatteryName }, (newState)=>{
																	this.setBatteryListPage();
																});
															}
														}
								/>
						  </Item>
							<View style={styles.refreshViewStyle}>
								<Button transparent success onPress={() => {
																this.setState({ machineBatteryName:'' }, (newState)=>{
																	this.setBatteryListPage();
																});
															}
														}>
									<Icon name="refresh" />
								</Button>
							</View>
          </Header>

          <ScrollView
            pagingEnabled={true}
            showsHorizontalScrollIndicator={true}
            bounces={true}
          >
            {this.renderHelper()}
          </ScrollView>
        </Container>

      );
    } else if (this.state.newCustomer) {
      return (
        <Content>
          <TouchableOpacity>
            <Thumbnail
              size={80} source={smallBatteryIcon}
              style={{ alignSelf: 'center', marginTop: 20, marginBottom: 10 }}
            />
          </TouchableOpacity>
          <Form>
            <Item floatingLabel>
              <Label>Machine</Label>
              <Input
                value={this.state.batteryName}
                onChangeText={batteryName => this.setState({ batteryName })}
              />
            </Item>
            <Item>
              <DatePicker
                style={{ width: 200 }}
                placeholder="Installed Date"
                date={this.state.installedDate}
                mode="date"
                format="YYYY-MM-DD"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: styles.dateIconStyle,
                  dateInput: {
                    marginLeft: 36,
                  },
                }}
                onDateChange={installedDate => this.setState({ installedDate })}
              />
            </Item>
            <Item floatingLabel>
              <Label>Battery Life (in days)</Label>
              <Input
                value={this.state.batteryLife}
                onChangeText={batteryLife => this.setState({ batteryLife })}
                keyboardType="numeric"
                maxLength={5}
              />
            </Item>
            <Item floatingLabel>
              <Label>Model</Label>
              <Input
                value={this.state.batteryModel}
                onChangeText={batteryModel => this.setState({ batteryModel })}
              />
            </Item>
            <Item floatingLabel>
              <Label>Battery Quantity</Label>
              <Input
                value={this.state.batteryQty}
                onChangeText={batteryQty => this.setState({ batteryQty })}
                keyboardType="numeric"
                maxLength={5}
              />
            </Item>
            <Item>
                  <Button
                    small rounded iconLeft success onPress={() => { this.onSaveBatteryInfo(); }}
                    style={styles.btn}>
                    <Icon name="recording" />
                    <Text> Save </Text>
                  </Button>
                  <Button
                    small rounded iconLeft warning onPress={() => { this.onBatteryInfoFormReset(); }}
                    style={styles.btn}>
                    <Icon name="refresh" />
                    <Text> Reset </Text>
                  </Button>
            </Item>
          </Form>
        </Content>
      );
    } else if (this.state.profilePage) {
      return (
        <Content>
          <TouchableOpacity>
            <Thumbnail
              size={80} source={smallBatteryIcon}
              style={styles.thumbnailStyle}
            />
          </TouchableOpacity>
          <List>
            <ListItem>
              <InputGroup>
                <Icon name="person" style={styles.iconStyle} />
                <Input
                  inlineLabel label="Name" placeholder="Name"
                  value={this.state.userName}
                  onChangeText={userName => this.setState({ userName })}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup>
                <Icon name="mail" style={styles.iconStyle} />
                <Input
                  placeholder="Email" value={this.state.emailId}
                  onChangeText={emailId => this.setState({ emailId })}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup>
                <Icon name="call" style={styles.iconStyle} />
                <Input
                  disabled
                  placeholder="Phone#"
                  value={this.state.phoneNumber}
                />
              </InputGroup>
            </ListItem>
            <ListItem iconLeft>
              <Icon name="transgender" style={styles.iconStyle} />
              <Text>Gender</Text>
              <Picker
                iosHeader="Select one"
                mode="dropdown"
								style={styles.pickerStyle}
                selectedValue={this.state.sex}
                onValueChange={sex => this.setState({ sex })}
              >
                <Item label="Male" value="Male" />
                <Item label="Female" value="Female" />
                <Item label="Transgender" value="Transgender" />
                <Item label="Other" value="Other" />
              </Picker>
            </ListItem>
            <ListItem>
              <InputGroup >
                <Input
                  stackedLabel label="Permanent Address" placeholder="Street Address, City, State"
                  value={this.state.addressStreetName}
                  onChangeText={addressStreetName => this.setState({ addressStreetName })}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup >
                <Input
                  placeholder="Zipcode"
                  value={this.state.cityZipcode}
                  keyboardType="numeric"
                  maxLength={10}
                  onChangeText={cityZipcode => this.setState({ cityZipcode })}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <Button
                small iconLeft rounded success onPress={() => { this.onSaveProfileInfo(); }}
                style={styles.btn}
              >
                <Icon name="recording" />
                <Text> Save </Text>
              </Button>
            </ListItem>
          </List>

        </Content>
      );
    }

    return (
      <View />
    );
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Body>
            <Title style={styles.text}> {'Utrakbatt'} </Title>
          </Body>

          <Right>
            <Button transparent onPress={() => this.props.navigation.navigate('Login')}>
              <Icon active name="exit" />
            </Button>
          </Right>
        </Header>

        <Content>
          { this.renderContent() }
        </Content>
        <Footer >
          <FooterTab>
            <Button active={this.state.batteryListPage} onPress={() => this.setBatteryListPage()} >
              <Icon name="battery-charging" />
              <Text>Batteries</Text>
            </Button>
            <Button active={this.state.newCustomer} onPress={() => this.setNewCustomerPage(true)} >
              <Icon name="add-circle" />
              <Text>New</Text>
            </Button>
            <Button active={this.state.profilePage} onPress={() => this.setProfilePage()} >
              <Icon name="person" />
              <Text>Profile</Text>
            </Button>
            <Button active={this.state.settingPage} onPress={() => this.setSettingPage()} >
              <Icon name="settings" />
              <Text>Settings</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

export default Home;
